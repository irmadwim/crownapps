<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('Api\v1')->group(function (){
    Route::group(['prefix' => 'v1'], function (){
        Route::post('/register', 'AuthController@register');
        Route::post('/login', 'AuthController@login');
        //Untuk User Controller
        Route::get('/readuser', 'UserController@read');
        Route::get('/showuser/{id}', 'UserController@getById');
        Route::post('/updateuser', 'UserController@update');
        Route::post('/updatepass', 'UserController@updatePassword');
        Route::post('/deleteuser', 'UserController@delete');
        //Untuk Anak Controller
        Route::post('/addanak', 'AnakController@add');
        Route::get('/readanak', 'AnakController@read');
        Route::post('/updateanak', 'AnakController@update');
        Route::post('/deleteanak', 'AnakController@delete');
        Route::get('/showanak/{id}', 'AnakController@getById');
        //Untuk Data Antropometri

        Route::post('/getdataanak', 'AnakController@getData');
        Route::post('/getdatatb', 'TbController@getDataTb');
        Route::post('/getdatabb', 'BbController@getDataBb');
        Route::post('/getdatalk', 'LkController@getDataLk');
        Route::post('/getdataimt', 'ImtController@getDataImt');
        Route::post('/getpertanyaan', 'PertanyaanController@getPertanyaan');

        Route::post('/updatereported', 'JwbRespController@updateReported');
        Route::post('/getreport', 'ReportAkhirController@getDataReport');

        Route::post('/getjawaban', 'JwbRespController@getData');
        Route::post('/getdatareport', 'ReportAkhirController@getData');
        Route::post('/getbb', 'BbController@getData');
        Route::post('/gettb', 'TbController@getData');
        Route::post('/getlk', 'LkController@getData');
        Route::post('/getimt', 'ImtController@getData');
        Route::get('/readtb', 'TbController@read');
        Route::get('/readbb', 'BbController@read');
        Route::get('/readlk', 'LkController@read');
        Route::get('/readimt', 'ImtController@read');

        //Untuk Aspek Controller
        Route::get('/readaspek', 'AspekController@read');
        Route::get('/showaspek/{id}', 'AspekController@getById');
        //Untuk Pertanyaan Controller
        Route::get('/readpertanyaan', 'PertanyaanController@read');
        Route::get('/showpertanyaan/{id}', 'PertanyaanController@getById');
        //Untuk Rekomendasi Gizi Controller
        Route::get('/readrekomgizi', 'RekomGiziController@read');
        //Untuk Data Gizi Controller
        Route::post('/updategizi', 'DataGiziController@update');
        Route::get('/showgizi/{id}', 'DataGiziController@getById');
        Route::post('/addgizi', 'DataGiziController@add');
        Route::get('/readgizi', 'DataGiziController@read');
        Route::post('/deletegizi', 'DataGiziController@delete');
        //Untuk Hasil Gizi Controller
        Route::post('/addhasil', 'HasilGiziController@add');
        Route::get('/readhasil', 'HasilGiziController@read');
        Route::get('/showhasil/{id}', 'HasilGiziController@getById');
        Route::post('/updatehasil', 'HasilGiziController@update');
        Route::post('/deletehasil', 'HasilGiziController@delete');
        //Untuk Report Akhir Controller
        Route::post('/addreport', 'ReportAkhirController@add');
        Route::get('/readreport', 'ReportAkhirController@read');
        Route::get('/showreport/{id}', 'ReportAkhirRespController@getById');
        Route::post('/updatereport', 'ReportAkhirController@update');
        Route::post('/deletereport', 'ReportAkhirController@delete');
        //Untuk Jawaban Responden Controller
        Route::post('/addjawaban', 'JwbRespController@add');
        Route::get('/readjawaban', 'JwbRespController@read');
        Route::get('/showjawaban/{id}', 'JwbRespController@getById');
        Route::post('/updatejawaban', 'JwbRespController@update');
        Route::post('/deletejawaban', 'JwbRespController@delete');

    });
});
