<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HasilGizi extends Model
{
    //
    protected $table = 'hasil_gizi';
    protected $fillable = [
        'id', 'gizi_id', 'hasil_tb', 'hasil_bb', 'hasil_lk', 'hasil_imt'
    ];
    public function datagizi() {
        return $this->belongsTo(DataGizi::class);
    }

    public $timestamps = false;
}
