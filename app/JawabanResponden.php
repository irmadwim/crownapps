<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JawabanResponden extends Model
{
    //
    protected $table = 'jawaban_responden';
    protected $fillable = [
        'id', 'aspek_id', 'anak_id', 'reported', 'jawaban_p', 'jawaban_f', 'jawaban_r', 'tanggal'
    ];
//    protected $attributes = [
//        'reported' => null,
//    ];
    public function anak() {
        return $this->belongsTo(Anak::class);
    }
    public function reportakhir() {
        return $this->hasMany(ReportAkhir::class);
    }
    public function aspek() {
        return $this->belongsTo(Aspek::class);
    }

    public $timestamps = false;
}

