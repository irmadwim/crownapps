<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PertanyaanDdst extends Model
{
    //
    protected $table = 'pertanyaan_ddst';
    protected $fillable = [
        'id', 'aspek_id', 'usia', 'pertanyaan'
    ];
    public function aspek() {
        return $this->belongsTo(Aspek::class);
    }
    public $timestamps = false;
}
