<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anak extends Model
{

    protected $table = 'anak';
    protected $fillable = [
        'id','user_id','nama_anak', 'tgl_lahir_anak', 'gender_anak'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }
    public function datagizi() {
        return $this->hasMany(DataGizi::class);
    }
    public function jawabanresponden() {
        return $this->hasMany(JawabanResponden::class);
    }
    public function reportakhir() {
        return $this->hasMany(User::class);
    }

    public $timestamps = false;

    //
}
