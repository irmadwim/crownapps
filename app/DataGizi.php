<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataGizi extends Model
{
    //
    protected $table = 'data_gizi';
    protected $fillable = [
        'id', 'anak_id', 'tanggal', 'tb_anak', 'bb_anak', 'lk_anak', 'imt_anak'
    ];
    public function anak() {
        return $this->belongsTo(Anak::class);
    }
    public function hasilgizi(){
        return $this->hasOne(HasilGizi::class);
    }
    public $timestamps = false;
}
