<?php


namespace App\Http\Controllers\Api\v1;


use App\Http\Controllers\Api\v1\BaseController as Controller;
use App\JawabanResponden;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class JwbRespController extends Controller
{
    public function read (){
        $data = JawabanResponden::all();
        return response([
            'jawabanresponden' => $data
        ], 200);
    }

    public function add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'jawaban_p' => 'required',
            'jawaban_f' => 'required',
            'jawaban_r' => 'required',
        ],
            [
                'jawaban_p.required' => 'Masukkan Jawaban',
                'jawaban_f.required' => 'Masukkan Jawaban',
                'jawaban_r.required' => 'Masukkan Jawaban',
            ]
        );

        if ($validator->fails()) {

            return response()->json([
                'success' => false,
                'message' => 'Silahkan Isi Bidang Yang Kosong',
                'data' => $validator->errors()
            ], 401);

        } else {

            $data = JawabanResponden::create([
                'aspek_id' => $request->aspek_id,
                'anak_id' => $request->anak_id,
                'reported' => $request->reported,
                'jawaban_p' => $request->jawaban_p,
                'jawaban_f' => $request->jawaban_f,
                'jawaban_r' => $request->jawaban_r,
                'tanggal' => Carbon::now(),
            ]);

            if ($data) {
                return response()->json([
                    'success' => true,
                    'message' => 'Data Berhasil Disimpan',
                ], 200);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Data Gagal Disimpan!',
                ], 403);
            }
        }
    }

    public function getById ($id){
        $data = JawabanResponden::with('anak', 'aspek')->find($id);
        if ($data) {
            return response()->json([
                'message' => 'Data Ditemukan',
                'data'    => $data
            ], 200);
        }
        else {
            return response()->json([
                'message' => 'Gagal! Data Tidak Ditemukan'
            ],404);
        }
    }

    public function delete (Request $request){
        $data = JawabanResponden::find($request->id);
        if($data){
            $data->delete();
            return response()->json([
                'message' => 'Data Berhasil Dihapus',

            ], 200);
        }
        return response()->json([
            'message' => 'Gagal'

        ], 400);

    }

    public function update (Request $request){
        $data = JawabanResponden::find($request->id);
        if ($data){
            $data = $data->update([
                'aspek_id' => $request->aspek_id,
                'anak_id' => $request->anak_id,
                'reported' => $request->reported,
                'jawaban_p' => $request->jawaban_p,
                'jawaban_f' => $request->jawaban_f,
                'jawaban_r' => $request->jawaban_r,
                'tanggal' => Carbon::now(),
            ]);
            return $this->sendResponse($data, 'Data berhasil diperbarui');
        }else {
            return $this->sendError('Gagal memperbarui data');
        }
        //}
    }

    public function updateReported (Request $request){
        $data = $request->all();
        $anak_id = $data['anak_id'];

        $data = JawabanResponden::where('anak_id', $anak_id)->get();
        foreach ($data as $item){
            $item->update([
                'reported' => 1,
            ]);
        }
//        else {
//            return $this->sendError('Gagal memperbarui data');
//        }
        return $this->sendResponse($data, 'Data berhasil diperbaharui');
        //}
    }

//    public function getData(Request $request){
//        $data = $request->all();
//        $anak = $data['anak_id'];
//        $tanggal = $data['tanggal'];
//
//        $anak = JawabanResponden::where(['anak_id' => $anak, 'tanggal' => $tanggal, 'reported' => null])->get();
//        if ($request->only(['anak_id', 'tanggal', 'reported'])) {
//            //
//            return response()->json([
//                'aspek' => $anak['aspek'],
//                'jawaban_p' => $anak['jawaban_p'],
//                'jawaban_f' => $anak['jawaban_f'],
//                'jawaban_r' => $anak['jawaban_r'],
//                ], 200);
//        } else {
//            return response()->json([
//                //'error' => 'true',
//                'message' => 'Gagal'
//            ], 403);
//        }
//    }

    public function getData(Request $request){
        $data = $request->all();
        $anak = $data['anak_id'];
        $tanggal = $data['tanggal'];

        $anak = JawabanResponden::where(['anak_id' => $anak, 'tanggal' => $tanggal, 'reported' => null])->get([
            'aspek_id',
            'jawaban_p',
            'jawaban_f',
            'jawaban_r',
        ]);
        return response()->json(
            $anak
            ,200);
    }
}
