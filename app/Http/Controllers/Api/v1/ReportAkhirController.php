<?php


namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\v1\BaseController as Controller;
use App\ReportAkhir;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ReportAkhirController extends Controller
{
    public function read (){
        $data = ReportAkhir::all();
        return response([
            'reportakhir' => $data
        ], 200);
    }

    public function add(Request $request){

            $data = ReportAkhir::create([
                'anak_id' => $request->anak_id,
                'hasil_akhir' => $request->hasil_akhir,
                'tanggal' => Carbon::now(),
            ]);

            if ($data) {
                return response()->json([
                    'success' => true,
                    'message' => 'Data Berhasil Disimpan',
                ], 200);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Data Gagal Disimpan!',
                ], 403);
            }
    }

    public function getById ($id){
        $data = ReportAkhir::with('anak', 'jawabanresponden')->find($id);
        if ($data) {
            return response()->json([
                'message' => 'Data Ditemukan',
                'data'    => $data
            ], 200);
        }
        else {
            return response()->json([
                'message' => 'Gagal! Data Tidak Ditemukan'
            ],404);
        }
    }

    public function delete (Request $request){
        $data = ReportAkhir::find($request->id);
        if($data){
            $data->delete();
            return response()->json([
                'message' => 'Data Berhasil Dihapus',

            ], 200);
        }
        return response()->json([
            'message' => 'Gagal'

        ], 400);

    }

    public function update (Request $request){
        $data = ReportAkhir::find($request->id);
        if ($data){
            $data = $data->update([
                'anak_id' => $request->anak_id,
                'hasil_akhir' => $request->hasil_akhir,
                'tanggal' => Carbon::now(),
            ]);
            return $this->sendResponse($data, 'Data berhasil diperbarui');
        }else {
            return $this->sendError('Gagal memperbarui data');
        }

    }

    public function getData(Request $request){
        $data = $request->all();
        $anak = $data['anak_id'];
        $tanggal = $data['tanggal'];

        $anak = ReportAkhir::where(['anak_id' => $anak, 'tanggal' => $tanggal])->get();
        if ($request->only(['anak_id', 'tanggal'])) {
            //
            return response()->json(
                $anak
                 , 200);
        } else {
            return response()->json([
                //'error' => 'true',
                'message' => 'Gagal'
            ], 403);
        }
    }

    public function getDataReport(Request $request){
        $data = $request->all();
        $anak = $data['anak_id'];
        $tanggal = $data['tanggal'];
        $data = explode('-',$tanggal);
        $tahun = $data[0];
        $bulan = $data[1];

        $anak = ReportAkhir::where('anak_id', $anak)->whereYear('tanggal', $tahun)->whereMonth('tanggal', $bulan)->get();
        if ($request->only(['anak_id', 'tanggal'])) {
            //
            return response()->json([
                'reportakhir' => $anak
            ], 200);
        } else {
            return response()->json([
                //'error' => 'true',
                'message' => 'Gagal'
            ], 403);
        }
    }
}
