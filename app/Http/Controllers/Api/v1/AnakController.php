<?php


namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\v1\BaseController as Controller;
use App\Anak;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AnakController extends Controller
{
    //mau nanya ini, apakah user id perlu dbuat validatornya juga?
    public function add(Request $request){
        $validator = Validator::make($request->all(), [
            'nama_anak'    => 'required',
            'tgl_lahir_anak'   => 'required',
            'gender_anak'  => 'required',
        ],
            [
                'nama_anak.required' => 'Masukkan Nama',
                'tgl_lahir_anak.required' => 'Masukkan Tanggal Lahir',
                'gender_anak.required' => 'Masukkan Jenis Kelamin',
            ]
        );

        if($validator->fails()) {

            return response()->json([
                'success' => false,
                'message' => 'Silahkan Isi Bidang Yang Kosong',
                'data'    => $validator->errors()
            ],401);

        }
        else {

            $data = Anak::create([
                'user_id' => $request->user_id,
                'nama_anak'     => $request->nama_anak,
                'tgl_lahir_anak'   => $request->tgl_lahir_anak,
                'gender_anak' => $request->gender_anak
            ]);

            if ($data) {
                return response()->json([
                    'error' => false,
                    'message' => 'Data Berhasil Disimpan',
                ], 200);
            } else {
                return response()->json([
                    'error' => true,
                    'message' => 'Data Gagal Disimpan!',
                ], 403);
            }
        }
    }

    public function read(){
        $anak = Anak::all();
        return response([
            'anak' => $anak
        ], 200);

    }

    public function getById ($id){
        $data = Anak::with('user')->find($id);
        if ($data) {
            return response()->json([
                'message' => 'Data Anak Ditemukan',
                'data'    => $data
            ], 200);
        }
        else {
            return response()->json([
                'message' => 'Gagal! Data Anak Tidak Ditemukan'
            ],404);
        }
    }

    public function update (Request $request){
        $validator = Validator::make($request->all(), [
            'nama_anak'    => ['required'],
            'tgl_lahir_anak'   => ['required'],
            'gender_anak'  => ['required'],
        ],
            [
                'nama_anak.required' => 'Masukkan Nama',
                'tgl_lahir_anak.required' => 'Masukkan Tanggal Lahir Baru',
                'gender_anak.required' => 'Masukkan Jenis Kelamin',
            ]
        );
        if($validator->fails()) {
            return $this->sendError('Gagal memperbarui data', $validator->errors(), 400);
        }else {
            $user = Anak::find($request->id);
            $user = $user->update([
                'user_id' => $request->user_id,
                'nama_anak' => $request->nama_anak,
                'tgl_lahir_anak' => $request->tgl_lahir_anak,
                'gender_anak' => $request->gender_anak,
            ]);
            return $this->sendResponse($user, 'Data berhasil diperbarui');
        }
    }

    public function delete (Request $request){
        $data = Anak::find($request->id);
        if($data){
            $data->delete();
            return response()->json([
                'message' => 'Data Berhasil Dihapus',

            ], 200);
        }
        return response()->json([
            'message' => 'Gagal'

        ], 400);

    }

    public function getData(Request $request){
        $data = $request->all();
        $ortu = $data['user_id'];

        $ortu = Anak::where('user_id', $ortu)->first();
        if ($request->only(['user_id'])) {
            //
            return response()->json(
                $ortu
                , 200);
        } else {
            return response()->json([
                //'error' => 'true',
                'message' => 'Gagal'
            ], 403);
        }
    }

}
