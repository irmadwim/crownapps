<?php


namespace App\Http\Controllers\Api\v1;


use App\Bb;
use App\Http\Controllers\Controller;
use App\Lk;
use Illuminate\Http\Request;

class LkController extends Controller
{
    public function read (){
        $data = Lk::all();
        return response([
            'lk' => $data
        ], 200);
    }

    public function getData(Request $request){
        $data = $request->all();
        $usia = $data['usia'];
        $jk = $data['jk'];

        $anak = Lk::where(['usia' => $usia, 'jk' => $jk])->first();
        if ($request->only(['usia', 'jk'])) {
            //
            return response()->json([
                'm3sd' => $anak['m3sd'],
                'm2sd' => $anak['m2sd'],
                'm1sd' => $anak['m1sd'],
                'medium' => $anak['medium'],
                'p1sd' => $anak['p1sd'],
                'p2sd' => $anak['p2sd'],
                'p3sd' => $anak['p3sd'],

            ], 200);
        } else {
            return response()->json([
                //'error' => 'true',
                'message' => 'Gagal'
            ], 403);
        }
    }

    public function getDataLk(Request $request){
        $data = $request->all();
        $jk = $data['jk'];

        $anak = Lk::where('jk', $jk)->get([
            'usia',
            'm3sd',
            'm2sd',
            'm1sd',
            'medium',
            'p1sd',
            'p2sd',
            'p3sd'
        ]);
        return response()->json(
            $anak
            ,200);
    }

}
