<?php


namespace App\Http\Controllers\Api\v1;


use App\DataGizi;
use App\Http\Controllers\Api\v1\BaseController as Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class DataGiziController extends Controller
{
    public function read (){
        $data = DataGizi::all();
        return response([
            'datagizi' => $data
        ], 200);
    }

    public function add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'tanggal' => 'required',
            'tb_anak' => 'required',
            'bb_anak' => 'required',
            'lk_anak' => 'required',
        ],
            [
                'tanggal.required' => 'Masukkan Tanggal',
                'tb_anak.required' => 'Masukkan Tinggi Badan',
                'bb_anak.required' => 'Masukkan Berat Badan',
                'lk_anak.required' => 'Masukkan Lingkar Kepala',
            ]
        );

        if ($validator->fails()) {

            return response()->json([
                'success' => false,
                'message' => 'Silahkan Isi Bidang Yang Kosong',
                'data' => $validator->errors()
            ], 401);

        } else {
            $data = DataGizi::create([
                'anak_id' => $request->anak_id,
                'tanggal' => $request->tanggal,
                'tb_anak' => $request->tb_anak,
                'bb_anak' => $request->bb_anak,
                'lk_anak' => $request->lk_anak,
                'imt_anak' => $request->imt_anak,
            ]);

            if ($data) {
                return response()->json([
                    'error' => false,
                    'message' => 'Data Berhasil Disimpan',
                ], 200);
            } else {
                return response()->json([
                    'error' => true,
                    'message' => 'Data Gagal Disimpan!',
                ], 403);
            }
        }
    }

    public function delete (Request $request){
        $data = DataGizi::find($request->id);
        if($data){
            $data->delete();
            return response()->json([
                'message' => 'Data Berhasil Dihapus',

            ], 200);
        }
        return response()->json([
            'message' => 'Gagal'

        ], 400);

    }

    public function getById ($id){
        $data = DataGizi::with('anak')->find($id);
        if ($data) {
            return response()->json([
                'message' => 'Data Ditemukan',
                'datagizi'    => $data
            ], 200);
        }
        else {
            return response()->json([
                'message' => 'Gagal! Data Tidak Ditemukan'
            ],404);
        }
    }

    public function update (Request $request){
        $validator = Validator::make($request->all(), [
            'tanggal' => ['required'],
            'tb_anak'    => ['required'],
            'bb_anak'   => ['required'],
            'lk_anak'  => ['required'],
        ],
            [
                'tanggal.required' => 'Masukkan Tanggal',
                'tb_anak.required' => 'Masukkan Tinggi Badan',
                'bb_anak.required' => 'Masukkan Berat Badan',
                'lk_anak.required' => 'Masukkan Lingkar Kepala',
            ]
        );
        if($validator->fails()) {
            return $this->sendError('Gagal memperbarui data', $validator->errors(), 400);
        }else {
            $data = DataGizi::find($request->id);
            $data = $data->update([
                'anak_id' => $request->anak_id,
                'tanggal' => $request->tanggal,
                'tb_anak' => $request->tb_anak,
                'bb_anak' => $request->bb_anak,
                'lk_anak' => $request->lk_anak,
                'imt_anak' => $request->imt_anak,
            ]);
            return $this->sendResponse($data, 'Data berhasil diperbarui');
        }
    }
    
}
