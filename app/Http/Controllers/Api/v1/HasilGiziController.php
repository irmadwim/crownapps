<?php


namespace App\Http\Controllers\Api\v1;


use App\DataGizi;
use App\HasilGizi;
use App\Http\Controllers\Api\v1\BaseController as Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HasilGiziController extends Controller
{
    public function read (){
        $data = HasilGizi::all();
        return response([
            'hasilgizi' => $data
        ], 200);
    }

    public function getById ($id){
        $data = HasilGizi::with('datagizi', 'anak')->find($id);
        if ($data) {
            return response()->json([
                'message' => 'Data Ditemukan',
                'data'    => $data
            ], 200);
        }
        else {
            return response()->json([
                'message' => 'Gagal! Data Tidak Ditemukan'
            ],404);
        }
    }

    public function add(Request $request){
        $data = HasilGizi::where('gizi_id', $request->gizi_id)->first();
        if(is_null($data)) {
            $data = HasilGizi::create([
                'gizi_id' => $request->gizi_id,
                'hasil_tb' => $request->hasil_tb,
                'hasil_bb' => $request->hasil_bb,
                'hasil_lk' => $request->hasil_lk,
                'hasil_imt' => $request->hasil_imt,
            ]);

            if ($data) {
                return response()->json([
                    'success' => true,
                    'message' => 'Data Berhasil Disimpan',
                ], 200);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Data Gagal Disimpan!',
                ], 403);
            }
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Data Double!',
            ], 403);
        }

    }

    public function delete (Request $request){
        $data = HasilGizi::find($request->id);
        if($data){
            $data->delete();
            return response()->json([
                'message' => 'Data Berhasil Dihapus',

            ], 200);
        }
        return response()->json([
            'message' => 'Gagal'

        ], 400);

    }

    public function update (Request $request){
//        $validator = Validator::make($request->all(), [
//            'tb_anak'    => ['required'],
//            'bb_anak'   => ['required'],
//            'lk_anak'  => ['required'],
//        ],
//            [
//                'tb_anak.required' => 'Masukkan Tinggi Badan',
//                'bb_anak.required' => 'Masukkan Berat Badan',
//                'lk_anak.required' => 'Masukkan Lingkar Kepala',
//            ]
//        );
//        if($validator->fails()) {
//            return $this->sendError('Gagal memperbarui data', $validator->errors(), 400);
//        }else {
            $data = HasilGizi::find($request->id);
            if ($data){
                $data = $data->update([
                    'gizi_id' => $request->gizi_id,
                    'hasil_tb' => $request->hasil_tb,
                    'hasil_bb' => $request->hasil_bb,
                    'hasil_lk' => $request->hasil_lk,
                    'hasil_imt' => $request->hasil_imt,
                ]);
                return $this->sendResponse($data, 'Data berhasil diperbarui');
            }else {
                return $this->sendError('Gagal memperbarui data');
            }

        //}
    }
}
