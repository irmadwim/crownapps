<?php


namespace App\Http\Controllers\Api\v1;


use App\Bb;
use App\Http\Controllers\Controller;
use App\PertanyaanDdst;
use Illuminate\Http\Request;

class PertanyaanController extends Controller
{
    public function read (){
        $data = PertanyaanDdst::all();
        return response([
            'pertanyaanddst' => $data
        ], 200);
    }
    public function getById ($id){
        $data = PertanyaanDdst::with('aspek')->find($id);
        if ($data) {
            return response()->json([
                'message' => 'Data Ditemukan',
                'data'    => $data
            ], 200);
        }
        else {
            return response()->json([
                'message' => 'Gagal! Data Tidak Ditemukan'
            ],404);
        }
    }

//    public function getPertanyaan(Request $request){
//        $data = $request->all();
//        $aspek_id = $data['aspek_id'];
//        $usia = $data['aspek_id'];
//
//        $anak = PertanyaanDdst::where(['aspek_id' => $aspek_id, 'usia' => $usia])->get();
//        if ($request->only(['aspek_id', 'usia'])) {
//            //
//            return response()->json([
//                'pertanyaanddst' => $anak
//            ], 200);
//        } else {
//            return response()->json([
//                //'error' => 'true',
//                'message' => 'Gagal'
//            ], 403);
//        }
//    }

    public function getPertanyaan(Request $request){
        $data = $request->all();
        $aspek_id = $data['aspek_id'];
        $usia = $data['usia'];

        $anak = PertanyaanDdst::where(['aspek_id' => $aspek_id, 'usia' => $usia])->get();
        if ($request->only(['aspek_id', 'usia'])) {
            //
            return response()->json([
                'pertanyaanddst' => $anak
            ], 200);
        } else {
            return response()->json([
                //'error' => 'true',
                'message' => 'Gagal'
            ], 403);
        }
    }

}
