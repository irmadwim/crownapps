<?php


namespace App\Http\Controllers\Api\v1;


use App\Http\Controllers\Api\v1\BaseController as Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function read (){
        $data = User::all();
        return response([
            'user' => $data
        ], 200);
    }

    public function getById ($id){
        $data = User::find($id);
        //$data = User::find($request->id_user);
        if ($data) {
            return response()->json([
                'message' => 'Pengguna Ditemukan',
                'data'    => $data
            ], 200);
        }
        else {
            return response()->json([
                'message' => 'Gagal! Pengguna Tidak Ditemukan'
            ],404);
        }
    }

    public function update (Request $request){

        $validator = Validator::make($request->all(), [
            'nama'     => 'required',
            'email'   => 'required',
        ],
            [
                'nama.required' => 'Masukkan Nama',
                'email.required' => 'Masukkan Email',
            ]
        );

        if($validator->fails()) {
            return $this->sendError('Gagal memperbarui data', $validator->errors(), 400);
        }else {
            $user = User::find($request->id);
                $user = $user->update([
                    'id' => $request->id,
                    'nama' => $request->nama,
                    'email' => $request->email,
                ]);
                return $this->sendResponse($user, 'Data berhasil diperbarui');
        }
    }

    public function delete (Request $request){
        $data = User::find($request->id);
        if($data){
            $data->delete();
            return response()->json([
                'message' => 'Data Berhasil Dihapus',

            ], 200);
        }
        return response()->json([
            'message' => 'Gagal'

        ], 400);

    }

    public function updatePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'current_password'    => ['required'],
            'new_password'   => ['required'],
            'confirm_new_password'  => ['required', 'same:new_password'],
        ],
            [
                'current_password.required' => 'Kata sandi sekarang tidak boleh kosong',
                'new_password.required' => 'Kata sandi baru tidak boleh kosong',
                'confirm_new_password.required' => 'Konfirmasi kata sandi baru tidak boleh kosong',
                'confirm_new_password.same' => "Kata sandi baru tidak sama, mohon periksa kembali",
            ]
        );
        if($validator->fails()) {
            return $this->sendError('Gagal mengubah kata sandi', $validator->errors(), 400);
        }else {
//            return response()->json(['login' => Auth::check()]);
            $user = User::find($request->id_user);
            if (!Hash::check($request->current_password, $user->password)){
                return $this->sendError('Kata sandi yang anda masukkan salah, harap periksa kembali', [], 400);
            }else if (Hash::check($request->new_password, $user->password)){
                return $this->sendError('Harap masukkan kata sandi baru yang berbeda', [], 400);
            } else {
                $user = $user->update([
                    'password' => Hash::make($request->new_password)
                ]);

                return $this->sendResponse($user, 'Kata sandi berhasil diperbarui');
            }
        }

    }
}
