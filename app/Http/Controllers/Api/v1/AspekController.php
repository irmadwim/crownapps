<?php


namespace App\Http\Controllers\Api\v1;


use App\Aspek;
use App\Http\Controllers\Controller;

class AspekController extends Controller
{
    public function read (){
        $data = Aspek::all();
        return response([
            'aspek' => $data
        ], 200);
    }

    public function getById ($id){
        $data = Aspek::find($id);
        //$data = User::find($request->id_user);
        if ($data) {
            return response()->json([
                'message' => 'Data Ditemukan',
                'data'    => $data
            ], 200);
        }
        else {
            return response()->json([
                'message' => 'Gagal! Data Tidak Ditemukan'
            ],404);
        }
    }

}
