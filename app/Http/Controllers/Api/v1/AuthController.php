<?php


namespace App\Http\Controllers\Api\v1;


use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function register (Request $request){
        //validate data
        $validator = Validator::make($request->all(), [
            'nama'    => 'required',
            'email'   => 'required',
            'password'  => 'required',
        ],
            [
                'nama.required' => 'Masukkan Nama',
                'email.required' => 'Masukkan Email',
                'password.required' => 'Masukkan Password',
            ]
        );

        if($validator->fails()) {

            return response()->json([
                'success' => false,
                'message' => 'Silahkan Isi Bidang Yang Kosong',
                'data'    => $validator->errors()
            ],401);

        }
        else {

            $data = User::create([
                'nama'     => $request->nama,
                'email'   => $request->email,
                'password' => Hash::make
                ($request->password)
            ]);

            if ($data) {
                return response()->json([
                    'success' => true,
                    'message' => 'Data Berhasil Disimpan',
                ], 200);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Data Gagal Disimpan!',
                ], 403);
            }
        }
    }
    public function login (Request $request){
        $data = $request->all();
        $email = $data['email'];
        $password = $data['password'];

        $activeUser = User::where('email', $email)->first();
        if($activeUser != null){
            //dd($activeUser);
            $credentials = $request->only(['email', 'password']);
            if (Auth::attempt($credentials)) {
                return response()->json([
                    'id' => $activeUser->id,
                    'nama' => $activeUser['nama'],
                    'email' =>  $activeUser['email'],
                    'password' =>  $activeUser['password'],
                    //'error' => 'false',
                    'message' => 'Berhasil'
                ],200);
            }else{
                return response()->json([
                    //'error' => 'true',
                    'message' => 'Gagal! Password Salah'
                ],403);
            }
        }else{
            return response()->json([
                //'error' => 'true',
                'message' => 'Gagal! Pengguna Tidak Ditemukan'
            ],500);
        }
    }
}
