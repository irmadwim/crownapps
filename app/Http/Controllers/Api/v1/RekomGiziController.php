<?php


namespace App\Http\Controllers\Api\v1;


use App\Http\Controllers\Controller;
use App\RekomendasiGizi;

class RekomGiziController extends Controller
{
    public function read (){
        $data = RekomendasiGizi::all();
        return response([
            'gizi' => $data
        ], 200);
    }

}
