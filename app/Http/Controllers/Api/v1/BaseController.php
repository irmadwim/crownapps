<?php


namespace App\Http\Controllers\Api\v1;


use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    public function sendResponse($result, $message){
        $response = [
            'error' => false,
            'message' => $message
        ];

        return response()->json($response, 200);
    }

    public function sendError($error, $errorMessage = [], $code = 404){
        $response = [
            'success' => false,
            'data' => $error,
        ];
        if (!empty($errorMessage)){
            $response['data'] = $errorMessage;
        }

        return response()->json($response, $code);
    }

}
