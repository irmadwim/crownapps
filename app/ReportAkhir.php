<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportAkhir extends Model
{
    //
    protected $table = 'report_akhir';
    protected $fillable = [
        'id', 'anak_id', 'hasil_akhir', 'tanggal'
    ];
    public function anak() {
        return $this->belongsTo(Anak::class);
    }
    public function jawabanresponden() {
        return $this->belongsTo(JawabanResponden::class);
    }

    public $timestamps = false;
}

